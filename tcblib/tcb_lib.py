import logging
from enum import IntEnum

import modbus_tk
import modbus_tk.defines as cst
import serial
from modbus_tk import modbus_rtu

RELAY0_COIL_ADDRESS = 10

RELAY1_COIL_ADDRESS = 20

RELAY2_COIL_ADDRESS = 30

RELAY3_COIL_ADDRESS = 40

J9_COIL_ADDRESS = 100

J9_IR_VOLTAGE_ADDRESS = 102
J9_IR_CURRENT_ADDRESS = 103

USB_COIL_ADDRESS = 200

USB_IR_VOLTAGE_ADDRESS = 202
USB_IR_CURRENT_ADDRESS = 203

DEV_INPUT_VOLTAGE_IR_ADDRESS = 300

DEV_FW_VER_MAJOR_IR_ADDRESS = 400
DEV_FW_VER_MINOR_IR_ADDRESS = 401
DEV_FW_VER_PATCH_IR_ADDRESS = 402

DEV_SCREEN_PAGE_HR_ADDRESS = 500
DEV_SCREEN_PAGE_IR_ADDRESS = 501
DEV_SCREEN_COUNT_IR_ADDRESS = 502

DEV_ACS_TAKE_COIL_ADDRESS = 510
DEV_ACS_IR_ADDRESS = 511

RELAY_COIL_INDEX_TO_ADDRESS = [
    RELAY0_COIL_ADDRESS,
    RELAY1_COIL_ADDRESS,
    RELAY2_COIL_ADDRESS,
    RELAY3_COIL_ADDRESS,
]

DIVIDER_CURRENT = 100.0
DIVIDER_VOLTAGE = 10.0

MODBUS_SLAVE_ID = 1


logger = logging.getLogger("tcb")
logger.setLevel(logging.ERROR)


class TestControlBoardError(IntEnum):
    OK = 0
    WRONG_PARAM = 10
    TIMEOUT = 20
    MODBUS = 30
    NOT_ACS = 40


class ControlSource(IntEnum):
    MANUAL = 0
    MODBUS = 1


class TestControlBoard:
    modbus: modbus_rtu.RtuMaster

    def __init__(self, uart: str) -> None:
        self.modbus = modbus_rtu.RtuMaster(
            serial.Serial(
                port=uart,
                baudrate=115200,
                bytesize=8,
                parity="N",
                stopbits=1,
                xonxoff=0,
            )
        )
        self.modbus.set_timeout(1.0)
        pass

    def __set_coil(self, address: int, on: bool) -> TestControlBoardError:
        err, acs = self.get_acs()
        if err == TestControlBoardError.OK:
            if acs != ControlSource.MODBUS:
                return TestControlBoardError.NOT_ACS
            return self.__set_coil_no_check(address, on)

    def __set_coil_no_check(self, address: int, on: bool) -> TestControlBoardError:
        try:
            self.modbus.execute(MODBUS_SLAVE_ID, cst.WRITE_SINGLE_COIL, address, output_value=on)
            return TestControlBoardError.OK

        except modbus_tk.modbus.ModbusError as exc:
            logger.error(f"{exc}- Code={exc.get_exception_code()}")
            return TestControlBoardError.MODBUS

    def __get_coil(self, address: int) -> tuple[TestControlBoardError, bool]:
        try:
            value = self.modbus.execute(MODBUS_SLAVE_ID, cst.READ_COILS, address, 1)
            return TestControlBoardError.OK, value

        except modbus_tk.modbus.ModbusError as exc:
            logger.error(f"{exc}- Code={exc.get_exception_code()}")
            return TestControlBoardError.MODBUS, False

    def __get_ir(self, address: int) -> tuple[TestControlBoardError, int]:
        try:
            value = self.modbus.execute(MODBUS_SLAVE_ID, cst.READ_INPUT_REGISTERS, address, 1)
            return TestControlBoardError.OK, value[0]

        except modbus_tk.modbus.ModbusError as exc:
            logger.error(f"{exc}- Code={exc.get_exception_code()}")
            return TestControlBoardError.MODBUS, 0

    def set_relay(self, index: int, on: bool) -> TestControlBoardError:
        if index <= len(RELAY_COIL_INDEX_TO_ADDRESS):
            return self.__set_coil(RELAY_COIL_INDEX_TO_ADDRESS[index], on)
        else:
            logger.error(f"Relay index out of bound: {index}, {len(RELAY_COIL_INDEX_TO_ADDRESS) - 1}")
            return TestControlBoardError.WRONG_PARAM

    def get_relay(self, index: int) -> tuple[TestControlBoardError, bool]:
        if index <= len(RELAY_COIL_INDEX_TO_ADDRESS):
            return self.__get_coil(RELAY_COIL_INDEX_TO_ADDRESS[index])
        else:
            logger.error(f"Relay index out of bound: {index}, {len(RELAY_COIL_INDEX_TO_ADDRESS) - 1}")
            return TestControlBoardError.WRONG_PARAM, False

    def set_j9(self, on: bool) -> TestControlBoardError:
        return self.__set_coil(J9_COIL_ADDRESS, on)

    def get_j9(self) -> tuple[TestControlBoardError, bool]:
        return self.__get_coil(J9_COIL_ADDRESS)

    def set_usb(self, on: bool) -> TestControlBoardError:
        return self.__set_coil(USB_COIL_ADDRESS, on)

    def get_usb(self) -> tuple[TestControlBoardError, bool]:
        return self.__get_coil(USB_COIL_ADDRESS)

    def get_input_voltage(self) -> tuple[TestControlBoardError, float]:
        err, val = self.__get_ir(DEV_INPUT_VOLTAGE_IR_ADDRESS)
        if err == TestControlBoardError.OK:
            val = val / DIVIDER_VOLTAGE
        return err, val

    def get_j9_voltage(self) -> tuple[TestControlBoardError, float]:
        err, val = self.__get_ir(J9_IR_VOLTAGE_ADDRESS)
        if err == TestControlBoardError.OK:
            val = val / DIVIDER_VOLTAGE
        return err, val

    def get_j9_current(self) -> tuple[TestControlBoardError, float]:
        err, val = self.__get_ir(J9_IR_CURRENT_ADDRESS)
        if err == TestControlBoardError.OK:
            val = val / DIVIDER_CURRENT
        return err, val

    def get_usb_voltage(self) -> tuple[TestControlBoardError, float]:
        err, val = self.__get_ir(USB_IR_VOLTAGE_ADDRESS)
        if err == TestControlBoardError.OK:
            val = val / DIVIDER_VOLTAGE
        return err, val

    def get_usb_current(self) -> tuple[TestControlBoardError, float]:
        err, val = self.__get_ir(USB_IR_CURRENT_ADDRESS)
        if err == TestControlBoardError.OK:
            val = val / DIVIDER_CURRENT
        return err, val

    def get_fw_version(self) -> tuple[TestControlBoardError, str]:
        err, major = self.__get_ir(DEV_FW_VER_MAJOR_IR_ADDRESS)
        if err != TestControlBoardError.OK:
            return err, ""
        err, minor = self.__get_ir(DEV_FW_VER_MINOR_IR_ADDRESS)
        if err != TestControlBoardError.OK:
            return err, ""
        err, patch = self.__get_ir(DEV_FW_VER_PATCH_IR_ADDRESS)
        if err != TestControlBoardError.OK:
            return err, ""

        return err, str(major) + "." + str(minor) + "." + str(patch)

    def take_over(self) -> TestControlBoardError:
        return self.__set_coil_no_check(DEV_ACS_TAKE_COIL_ADDRESS, True)

    def get_acs(self) -> tuple[TestControlBoardError, ControlSource]:
        err, acs = self.__get_ir(DEV_ACS_IR_ADDRESS)
        return err, ControlSource(acs)
