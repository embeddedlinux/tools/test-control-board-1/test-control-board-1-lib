#!/usr/bin/env python3

import argparse
from importlib.metadata import distribution

from tcb_lib import TestControlBoard, TestControlBoardError

SWITCHABLE_TARGET_NAMES = "relay0 relay1 relay2 relay3 j9 usb"
CMEAS_TARGET_NAMES = "j9 usb"
VMEAS_TARGET_NAMES = "j9 usb in"


def process_set_coil(tcb: TestControlBoard, name: str, on: bool):
    err = TestControlBoardError.OK
    match name:
        case "relay0":
            err = tcb.set_relay(0, on)
        case "relay1":
            err = tcb.set_relay(1, on)
        case "relay2":
            err = tcb.set_relay(2, on)
        case "relay3":
            err = tcb.set_relay(3, on)
        case "j9":
            err = tcb.set_j9(on)
        case "usb":
            err = tcb.set_usb(on)
        case _:
            print(f"wrong target name, valid target names are: {SWITCHABLE_TARGET_NAMES}")
            return False
    if err != TestControlBoardError.OK:
        print(f"Failed: {TestControlBoardError(err).name}")


def process_get_coil(tcb: TestControlBoard, name: str) -> bool:
    err = TestControlBoardError.OK
    match name:
        case "relay0":
            err, value = tcb.get_relay(0)
        case "relay1":
            err, value = tcb.get_relay(1)
        case "relay2":
            err, value = tcb.get_relay(2)
        case "relay3":
            err, value = tcb.get_relay(3)
        case "j9":
            err, value = tcb.get_j9()
        case "usb":
            err, value = tcb.get_usb()
        case _:
            print(f"wrong target name, valid target names are: {SWITCHABLE_TARGET_NAMES}")
            return False
    if err != TestControlBoardError.OK:
        print(f"Failed: {TestControlBoardError(err).name}")
        return False
    return value[0]


def process_get_current(tcb: TestControlBoard, name: str) -> float:
    err = TestControlBoardError.OK
    match name:
        case "j9":
            err, value = tcb.get_j9_current()
        case "usb":
            err, value = tcb.get_usb_current()
        case _:
            print(f"wrong target name, valid target names are: {CMEAS_TARGET_NAMES}")
            return 0.0

    if err != TestControlBoardError.OK:
        print(f"Failed: {TestControlBoardError(err).name}")
        return 0.0
    return value


def process_get_voltage(tcb: TestControlBoard, name: str) -> float:
    err = TestControlBoardError.OK
    match name:
        case "j9":
            err, value = tcb.get_j9_voltage()
        case "usb":
            err, value = tcb.get_usb_voltage()
        case "in":
            err, value = tcb.get_input_voltage()
        case _:
            print(f"wrong target name, valid target names are: {VMEAS_TARGET_NAMES}")
            return 0.0

    if err != TestControlBoardError.OK:
        print(f"Failed: {TestControlBoardError(err).name}")
        return 0.0
    return value


# later on, maybe other printers will be implemented..
def value_printer(value):
    print(value)


def main():
    dist = distribution("tcblib")
    tcb_version = dist.version

    parser = argparse.ArgumentParser(
        prog="tcb_tool",
        description="tool for controlling:\
            https://gitlab.com/embeddedlinux/tools/test-control-board-1/test-control-board",
    )

    parser.add_argument("port", help="RTU serial port")
    parser.add_argument("-s", "--set", help=f"set output ON, valid output names: {SWITCHABLE_TARGET_NAMES}")
    parser.add_argument("-c", "--clear", help=f"set output OFF, valid output names: {SWITCHABLE_TARGET_NAMES}")
    parser.add_argument("-g", "--get", help=f"get output state, valid output names: {SWITCHABLE_TARGET_NAMES}")
    parser.add_argument("-u", "--get-voltage", help=f"get voltage, valid targets: {VMEAS_TARGET_NAMES}")
    parser.add_argument("-i", "--get-current", help=f"get voltage, valid targets: {CMEAS_TARGET_NAMES}")
    parser.add_argument("-a", "--acs", help="read active control source", action="store_true")
    parser.add_argument(
        "--take-over",
        help="request modbus as active control source. Think twice before using from script",
        action="store_true",
    )
    parser.add_argument("--fw-version", help="read firmware version from board", action="store_true")
    parser.add_argument(
        "-V", "--version", help="display tcb-tool/lib version", action="version", version=f"tcb_tool {tcb_version}"
    )

    args = parser.parse_args()

    tcb = TestControlBoard(args.port)

    if args.set is not None:
        process_set_coil(tcb, args.set, True)

    if args.clear is not None:
        process_set_coil(tcb, args.clear, False)

    if args.get is not None:
        on = process_get_coil(tcb, args.get)
        value_printer(on)

    if args.get_current is not None:
        curr = process_get_current(tcb, args.get_current)
        value_printer(curr)

    if args.get_voltage is not None:
        volt = process_get_voltage(tcb, args.get_voltage)
        value_printer(volt)

    if args.fw_version:
        err, ver = tcb.get_fw_version()
        if err != TestControlBoardError.OK:
            print(f"Failed: {TestControlBoardError(err).name}")
        value_printer(ver)

    if args.take_over:
        err = tcb.take_over()
        if err != TestControlBoardError.OK:
            print(f"Failed: {TestControlBoardError(err).name}")

    if args.acs:
        err, acs = tcb.get_acs()
        if err != TestControlBoardError.OK:
            print(f"Failed: {TestControlBoardError(err).name}")

        value_printer(acs.name)


if __name__ == "__main__":
    main()
