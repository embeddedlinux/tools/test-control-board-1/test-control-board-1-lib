# tcb-tool

## Installation

```bash
pip install tcblib --index-url https://gitlab.com/api/v4/projects/44052166/packages/pypi/simple
```

### From sources

```bash
python3 -m venv venv
. venv/bin/activate
pip install .[dev]
```
