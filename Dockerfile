FROM python:3.10-slim

RUN pip install -U pip
RUN pip install twine

# COPY pyproject.toml .
# RUN pip install .[dev]

RUN pip install black
RUN pip install build
RUN pip install isort[colors]
RUN pip install mypy
RUN pip install pylint
RUN pip install types-pyserial

